from flask import Flask, render_template
from downloader import main as load_map
from json import load

app = Flask(__name__, static_url_path='/static')

# def load_map():
#     map_json = open('static/map.json', 'r')
#     tile_map = load(map_json)
#     return tile_map


@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r

@app.route('/')
def index():
    map_data = load_map()
    return render_template('index.html', map_data=map_data)
