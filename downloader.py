from __future__ import print_function
from apiclient import discovery

def load_data(spreadsheet, range, service):
    result = service.spreadsheets().get(
        spreadsheetId=spreadsheet, range=range, valueRenderOption='UNFORMATTED_VALUE').execute()
    return result

def main():
    print("Connecting to Google Sheets API...")
    file = open('api_key.txt', "r")
    api_key = file.read()
    print(api_key)
    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    service = discovery.build('sheets', 'v4', discoveryServiceUrl=discoveryUrl, developerKey=api_key)
    spreadsheetId = '1pqtRASueyGVE1sjyqqQdMXsTg7V5ionQLogEy9o64fU'
    result = service.spreadsheets().get(
        spreadsheetId=spreadsheetId, includeGridData = "true").execute()
    tile_map = []

    x_dimension = (result["sheets"][0]["properties"]["gridProperties"]["columnCount"])
    y_dimension = (result["sheets"][0]["properties"]["gridProperties"]["columnCount"])

    values = result["sheets"][0]["data"][0]["rowData"]

    widths = []
    for width in result["sheets"][0]["data"][0]["columnMetadata"]:
        widths.append(width['pixelSize'])

    heights = []
    for height in result["sheets"][0]["data"][0]["rowMetadata"]:
        heights.append(height['pixelSize'])
    print(heights)

    tiles = []
    for row in values:
        for cell in row["values"]:
            try:
                tiles.append(cell["userEnteredValue"]["stringValue"])
            except(KeyError):
                tiles.append("EMPTY")
    i = 0
    cur_height = 0
    for height in heights:
        cur_width = 0
        for width in widths:
            tile = dict()
            tile["pos_x"] = cur_width
            tile["pos_y"] = cur_height
            tile["tile_width"] = width
            tile["tile_height"] = height
            tile["type"] = tiles[i]
            tile_map.append(tile)
            i += 1
            cur_width += width
        cur_height += height
    return tile_map


if __name__ == '__main__':
    main()
