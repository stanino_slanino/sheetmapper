class CollisionDetector {

  CollideProjectilesWithTiles(projectile, tileArray) {
    for (let j = 0; j<tileArray.length; j++) {
      let tile = tileArray[j];
      let nextFramePos = p5.Vector.add(projectile.position, projectile.movement);
      // let hit = collidePointRect(projectile.position.x, projectile.position.y, tile.pos_x, tile.pos_y, tile.tile_width, tile.tile_height)
      let hit = collideLineRect(projectile.position.x, projectile.position.y, nextFramePos.x, nextFramePos.y, tile.pos_x, tile.pos_y, tile.tile_width, tile.tile_height, true)
      if (hit.top.x || hit.bottom.x || hit.left.x || hit.right.x) {
        return { tile:tile, hit:hit };
      }
    }
    return null;
  }

  CollideProjectileWithPlayers(projectile, players) {
    let hit = false;
    let nextFramePos = p5.Vector.add(projectile.position, projectile.movement);
    for (let i = 0; i<players.length; i++) {
      let playerPos = players[i].position;
      hit = collidePointCircle(nextFramePos.x, nextFramePos.y, playerPos.x, playerPos.y, playerSize);
      if (hit) return players[i];
    }
    return hit;
  }

  CollidePlayerWithTiles(player, tileArray) {
    let hit = false;
    let nextFramePos = p5.Vector.add(player.position, player.velocity);
    for (let i = 0; i<tileArray.length; i++) {
      let tile = tileArray[i];
      hit = collideRectCircle(tile.pos_x, tile.pos_y, tile.tile_width, tile.tile_height, nextFramePos.x, nextFramePos.y, playerSize);
      if (hit) break;
      }
      return hit;
    }

  CollidePlayerWithMoveBounds(player) {
    let hit = false;
    let nextFramePos = p5.Vector.add(player.position, player.velocity);
    hit = collideCircleCircle(nextFramePos.x, nextFramePos.y, playerSize, player.positionOnTurnStart.x, player.positionOnTurnStart.y, player.moveRadius - 1.5*playerSize);
    return hit;
    }
  }
