class Tile
{
  constructor(x, y, width, height, type)
  {
    this.pos_x = x;
    this.pos_y = y;
    this.tile_width = width;
    this.tile_height = height;
    this.type = type;
    if (this.type === "TITANIUM") {
      this.hp = 10000;
    }
    else {
      this.hp = baseTileHP;
    }
    this.isDead = false;
  }

  RemoveHP(change) {
    this.hp -= change;
    this.CheckHP();
  }

  CheckHP() {
    if (this.hp <= 0) {
      this.DestroyTile();
    }
  }

  DrawTile()
  {
    push();
      switch(this.type)
      {
        case "GROUND":
          fill(139,69,19);
          break;
        case "TITANIUM":
          fill(0,0,0);
          break;
        {
          fill(0);
        }
      }
      rect(this["pos_x"], this["pos_y"], this["tile_width"], this["tile_height"]);
    pop()
  }

  DestroyTile()
  {
    this.isDead = true;
  }
}
