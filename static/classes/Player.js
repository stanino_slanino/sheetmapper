class Player {
  constructor(x, y, id) {
    this.position = createVector(x, y);
    this.positionOnTurnStart = this.position.copy();
    this.moveRadius = basicMoveRadius;
    this.velocity = createVector(0,0);
    this.currentReflectCount = baseShotHP;
    this.maxReflectCount = maxReflectCount;
    this.id = id;
    this.canShoot = false;
    this.canMove = false;
  }

  DrawPlayer() {
    push();
    fill(255);
    if (this === turnController.activePlayer) {
      fill(0);
    }
    ellipse(this.position.x, this.position.y, playerSize);
    fill(255,0,0,20);
    if (this === turnController.activePlayer) {
      ellipse(this.positionOnTurnStart.x, this.positionOnTurnStart.y, this.moveRadius);
    }
    pop();
  }

  SetVelocity() {
    if (keyIsDown(LEFT_ARROW) || keyIsDown(RIGHT_ARROW) || keyIsDown(UP_ARROW) || keyIsDown(DOWN_ARROW) || keyIsDown(65) || keyIsDown(68) || keyIsDown(87) || keyIsDown(87) || keyIsDown(83)) {
      camera.SetNextCamera(this.position);
      if (keyIsDown(LEFT_ARROW) || keyIsDown(65))
        this.velocity.x = -playerSpeed;
      if (keyIsDown(RIGHT_ARROW) || keyIsDown(68))
        this.velocity.x = playerSpeed;
      if (keyIsDown(UP_ARROW) || keyIsDown(87))
        this.velocity.y = -playerSpeed;
      if (keyIsDown(DOWN_ARROW) || keyIsDown(83))
        this.velocity.y = playerSpeed;
    }
    else {
      this.velocity.x = 0;
      this.velocity.y = 0;
    }

    if (this.position.x < 0) {
      this.velocity.x = 0;
      this.position.x = 1;
    }
  }

  MovePlayer() {
    if (!collisionDetector.CollidePlayerWithTiles(this, tileController.arr)
        && collisionDetector.CollidePlayerWithMoveBounds(this)
        && this.canMove)
      this.position.add(this.velocity);
  }

  Shoot(destination) {
    new Shot("player", this.position.copy(), destination, this.currentReflectCount);
    this.canShoot = false;
    this.canMove = false;
    gameStatus.ChangeStatus("waiting")
  }

  SetMovementConstraint() {
    this.positionOnTurnStart = this.position.copy();
    this.moveRadius = basicMoveRadius;
  }

  ToggleReflectionNumber() {
    if (this.currentReflectCount < this.maxReflectCount) {
      this.currentReflectCount++;
    }
    else {
      this.currentReflectCount = baseShotHP;
    }
  }

  EnableShooting() {
    this.canShoot = true;
  }

  EnableMovement() {
    this.canMove = true;
  }
}
