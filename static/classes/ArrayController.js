class ArrayController {
  constructor() {
    this.arr = [];
  }

  GetLongestDistance() {
    let longestDistance = 0;
    for (let i = 0; i<this.arr.length; i++) {
      for (let j = 0; j<this.arr.length; i++) {
        if (i=j) {
          continue;
        }
        else {
          let distance = dist(this.arr[i].position,this.arr[j].position);
          if (distance > longestDistance) {
            longestDistance = distance;
          }
        }
      }
    }
    return longestDistance;
  }

  GetNearestMember(member) {
    let nearestMember;
    let nearestDistance;

    if (this.arr.length <=1) {
        return { member : member, distance : 0 };
    }

    if (member !== this.arr[0]) {
        nearestDistance = member.position.dist(this.arr[0].position);
        nearestMember = this.arr[0];
    }
    else {
        nearestDistance = member.position.dist(this.arr[1].position);
        nearestMember = this.arr[1];
    }

    if (this.arr.length > 2) {
        for (let i = 0; i < this.arr.length; i++) {
            let distance = member.position.dist(this.arr[i].position);
            if (distance < nearestDistance) {
                nearestDistance = distance;
                nearestMember = this.arr[i];
            }
        }
    }
    return { member : nearestMember, distance : nearestDistance };
  }

  AddToArray(object) {
    this.arr.push(object);
  }

    RemoveDead() {
    if (this.arr.length !== 0) {
      for (let i = this.arr.length - 1; i >= 0; i--) {
        if (this.arr[i].isDead === true) {
          this.arr.splice(i, 1);
        }
      }
    }
  }
}
