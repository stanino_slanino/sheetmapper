class Camera {
  constructor() {
    this.position = createVector(0,0);
    this.nextPosition = createVector(0,0);
  }

  UpdateCamera() {
    this.position.x = lerp(this.position.x, this.nextPosition.x, 0.05);
    this.position.y = lerp(this.position.y, this.nextPosition.y, 0.05);
    translate(this.position.x, this.position.y);
  }

  SetNextCamera(position) {
    let centralPoint = createVector(windowWidth/2, windowHeight / 2);
    if (position.x < centralPoint.x && position.y < centralPoint.y) {
        console.log("camera mode: corner");
      if (position.x < centralPoint.x) {
        this.nextPosition.x = 0;
      }
      if (position.y < centralPoint.y) {
          this.nextPosition.y = 0;
      }
      return;
    }

    let activePlayer = turnController.activePlayer;
    let nearestPlayer = players.GetNearestMember(activePlayer);
    if (abs(nearestPlayer.member.position.x - activePlayer.position.x) < windowWidth
        && abs(nearestPlayer.member.position.y - activePlayer.position.y) < windowHeight) {
        console.log("camera mode: close");
        let middlePoint = p5.Vector.lerp(activePlayer.position, nearestPlayer.member.position, 0.5);
        this.nextPosition = p5.Vector.mult(middlePoint, -1).add(centralPoint);
        return;
    }
      console.log("camera mode: distant");
    this.nextPosition = p5.Vector.mult(position, -1).add(centralPoint);
  }
}
