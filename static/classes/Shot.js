class Shot {

  constructor(type, position, destination, hp) {
    this.createFrame = frameCount;
    this.isDead = false;
    this.movement = destination.copy().sub(position);
    this.movement.normalize();
    this.movement.mult(shotVelocity);
    this.position = p5.Vector.add(position, this.movement);
    this.type = type;
    this.hp = hp;
    shotController.AddToArray(this);
  }

  MoveShot() {
    let hitTile = collisionDetector.CollideProjectilesWithTiles(this, tileController.arr);
    let hitPlayer = collisionDetector.CollideProjectileWithPlayers(this, players.arr);
    if (hitPlayer) {
      this.DestroyShot();
      gameStatus.ChangeStatus("GAME OVER");
      noLoop();
    }
    if (!hitTile) {
      this.position.add(this.movement);
    }
    else {
      let hit = hitTile.hit;
      if (hit.top.x) {
        this.movement.y = -this.movement.y;
        this.position = p5.Vector.add(createVector(hit.top.x, hit.top.y), this.movement);
      }
      if (hit.bottom.x) {
        this.movement.y = -this.movement.y;
        this.position = p5.Vector.add(createVector(hit.bottom.x, hit.bottom.y), this.movement);
      }
      if (hit.left.x) {
        this.movement.x = -this.movement.x;
        this.position = p5.Vector.add(createVector(hit.left.x, hit.left.y), this.movement);
      }
      if (hit.right.x) {
        this.movement.x = -this.movement.x;
        this.position = p5.Vector.add(createVector(hit.right.x, hit.right.y), this.movement);
      }
      this.RemoveHP(1);
      this.CheckHP();
      hitTile.tile.RemoveHP(1);
    }
  }

  DrawShot() {
    ellipse(this.position.x, this.position.y, shotSize);
  }

  CheckLifetime() {
    if (this.createFrame + shotLifespan <= frameCount) {
      this.DestroyShot();
    }
  }

  CheckHP() {
    if (this.hp <= 0) {
      this.DestroyShot();
    }
  }

  RemoveHP(change) {
    this.hp -= change;
  }

  DestroyShot() {
    this.isDead = true;
    turnController.ChangeActivePlayer();
    gameStatus.ChangeStatus("movement");
  }
}
