class Crosshair {
    constructor() {
        this.position = createVector(mouseX, mouseY).sub(camera.position);
    }

    DrawCrosshair(player) {
        this.position.x = mouseX - camera.position.x;
        this.position.y = mouseY - camera.position.y;
        push();
        fill('red');
        ellipse(this.position.x, this.position.y, 5);
        stroke(255,0,0,80);
        line(player.position.x, player.position.y, this.position.x, this.position.y);
        pop();
    }
}