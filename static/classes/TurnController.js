class TurnController {
  constructor() {
    this.activePlayer = players.arr[0];
    this.activePlayer.EnableShooting();
    this.activePlayer.EnableMovement();
    this.turnNumber = 1;
  }

  ChangeActivePlayer() {
    let index = players.arr.indexOf(this.activePlayer)
    if (index == players.arr.length - 1) {
      this.activePlayer = players.arr[0];
      this.SetNextRound();
    }
    else {
      this.activePlayer = players.arr[index+1];
    }
    this.activePlayer.EnableShooting();
    this.activePlayer.EnableMovement();
    this.activePlayer.SetMovementConstraint();
    camera.SetNextCamera(this.activePlayer.position);
  }

  SetNextRound() {
    this.turnNumber += 1;
  }
}
