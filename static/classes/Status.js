class Status {
  constructor() {
    this.currentStatus = "movement";
  }

  ChangeStatus(status) {
    this.currentStatus = status;
  }

  DrawStatus()
  {
    push();
      fill(255);
      stroke(0);
      text("Turn number: " + turnController.turnNumber, 10, 10);
      text("Active player: " + turnController.activePlayer.id, 100, 10);
      text("Current status: " + this.currentStatus, 200, 10);
      text("Reflections (Q to toggle): " + turnController.activePlayer.currentReflectCount, 400, 10);
    pop();
  }

  DrawErrors(errors)
  {
    for (let i = 0; i < errors.length; i++){
      text(errors[i], 10, 15 + i*15);
    }
  }
}
