
let map_json;
let map_errors = [];
let tileController;
let shotController;
let turnController;
let players;
let collisionDetector;
let activePlayer;
let camera;
let gameStatus;
let crosshair;
const shotVelocity = 7;
const shotSize = 3;
const shotLifespan = 120;
const playerSpeed = 3;
const numberOfPlayers = 2;
const playerSize = 15;
const basicMoveRadius = 140;
const baseShotHP = 2;
const maxReflectCount = 5;
const baseTileHP = 1;
let starts = [];
let maxCameraSpread = 0;


function preload()
{
  map_json = JSON.parse(map_data);
}

function setup()
{
  shotController = new ArrayController;
  tileController = new ArrayController;
  players = new ArrayController;
  collisionDetector = new CollisionDetector;
  camera = new Camera();
  crosshair = new Crosshair();
  createCanvas(windowWidth - 20, windowHeight - 20);
  background(255);
  noFill();
  stroke(0);
  for (let tile of map_json) {
    if (tile["type"] !== "EMPTY") {
      if (tile["type"] === "START") {
        let start = {pos_x : tile.pos_x + tile.tile_width/2, pos_y : tile.pos_y + tile.tile_height/2};
        starts.push(start);
      }
      else {
        tile = new Tile(tile["pos_x"], tile["pos_y"], tile["tile_width"], tile["tile_height"], tile["type"]);
        tileController.arr.push(tile);
      }
    }
  }
  gameStatus = new Status;
  // should assert >2 starting positions
  if (numberOfPlayers <= starts.length) {
    for (let i = 0; i<numberOfPlayers; i++){
        let player = new Player(starts[i].pos_x, starts[i].pos_y, i+1);
        players.arr.push(player);
    }
    turnController = new TurnController;
  }
  else {
    map_errors.push("Not enough start tiles.")
  }
  camera.SetNextCamera(turnController.activePlayer.position);
}

//p5.js function that draws the image
function draw()
{
  background(255);

  if (map_errors.length > 0) {
    gameStatus.DrawErrors(map_errors);
    noLoop();
    return;
  }


  activePlayer = turnController.activePlayer;
  push();
  camera.UpdateCamera();
  activePlayer.SetVelocity();
  activePlayer.MovePlayer();


  for (let tile of tileController.arr)
  {
    tile.DrawTile()
  }
  for (let player of players.arr)
  {
    player.DrawPlayer();
  }


  for (let i = 0; i<shotController.arr.length; i++)
  {
    let shot = shotController.arr[i];
    shot.MoveShot();
    shot.DrawShot();
    shot.CheckLifetime();
  }
  crosshair.DrawCrosshair(activePlayer);
  pop();



  shotController.RemoveDead();
  tileController.RemoveDead();
  gameStatus.DrawStatus();
}

function mouseClicked()
{
  if (activePlayer.canShoot) {
    activePlayer.Shoot(crosshair.position);
  }
}

function keyPressed() {
  if (keyCode === 81)
    activePlayer.ToggleReflectionNumber();
}

function windowResized()
{
    resizeCanvas(windowWidth, windowHeight);
    maxCameraSpread = sqrt(sq(windowWidth) + sq(windowHeight));
    camera.SetNextCamera(turnController.activePlayer.position);
}
